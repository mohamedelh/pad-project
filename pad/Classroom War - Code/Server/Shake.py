import bge
from bge import logic
import random

def main():
    #get controller
    con = bge.logic.getCurrentController()
    #get playerp
    camera = con.owner
    #get scene
    scene = logic.getCurrentScene()
    #get all objects in the scene
    objList = scene.objects
    
    #create lists
    chairList = []
    numberListX = []
    numberListY = []
    numberListZ = []
    
    for gobj in objList:
            if gobj.name == "Chair":
                #add chairs from scene
                chairList.append(gobj)
                #per chair generate a random number
                numberListX.append(random.uniform(-5, 5))
                numberListY.append(random.uniform(-5, 5))
                numberListZ.append(random.uniform(4, 8))
    
    randomTime = random.randint(0, 1500)
    
    i = 0
    if randomTime == 2:
        for chair in chairList:
            #shake doesn't affect chairs in the air
            if chair.worldPosition.z < 1:
                chair.setLinearVelocity([numberListX[i], numberListY[i], numberListZ[i]], False)
                i += 1
        
        #setup code for shaking camera
        camera["shakeCamera"] = True
        camera["cameraShake"] = 1
    
    
    #shake camera
    if camera["shakeCamera"] == True:
        camera["cameraShake"] = -camera["cameraShake"] * random.uniform(0.5, 0.8)
        
        camera.applyMovement([camera["cameraShake"], 0, 0], True)
        
        #stop shaking camera if number gets low
        if (camera["cameraShake"] < 0.07 and camera["cameraShake"] > 0) or (camera["cameraShake"] > -0.07 and camera["cameraShake"] < 0):
            camera.worldPosition = [23, 0, 19]
            camera["shakeCamera"] = False

main()