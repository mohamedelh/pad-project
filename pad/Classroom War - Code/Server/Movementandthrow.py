from bge import logic, types, events
from mathutils import Vector
import os
import sys
from bpy import context
import math

class MovementAndThrow(types.KX_GameObject):
    
    def __init__(self, own):
        
        self.entity = self["entity"]
        
    def main(self):
        
        k = self.entity.keyboard.keyDown
        j = self.entity.joystick
        mPosition = self.entity.mouse.mousePosition
        mouseX = mPosition[0]
        
        #setup animation
        self["playerWalking"] = False
        self["playerWalkBack"] = False
        self["idleStanding"] = False
        self["idleChair"] = False
        self["walkingChair"] = False
        self["backWalkingChair"] = False
        
        #JOYSTICK MOVEMENT
        motion = [j.lLR, j.lUD, 0.0]
        facing = [0.0, 0.0, -j.rLR]
        
        if abs(j.lLR) > j.axisTh or abs(j.lUD) > j.axisTh:
            self.applyMovement(motion, True)
            if not j.leftTrigger > 0:
                self["playerWalking"] = True
        
        #apply rotation
        self.applyRotation(facing, True)
        
        
        #KEYBOARD MOVEMENT
        #left
        if k(events.AKEY):
            self.applyMovement((0.1, 0, 0), True)
            if not k(events.SPACEKEY):
                self["playerWalking"] = True
        #right
        if k(events.DKEY):
            self.applyMovement((-0.1, 0, 0), True)
            if not k(events.SPACEKEY):
                self["playerWalking"] = True
        #forwards
        if k(events.WKEY):
            self.applyMovement((0, -0.1, 0), True)
            if not k(events.SPACEKEY):
                self["playerWalking"] = True
        #backwards
        if k(events.SKEY):
            self.applyMovement((0, 0.1, 0), True)
            if not k(events.SPACEKEY):
                self["playerWalkBack"] = True
        
        if not k(events.SKEY) and not k(events.DKEY) and not k(events.WKEY) and not k(events.AKEY) and not k(events.SPACEKEY) and not (abs(j.lLR) > j.axisTh or abs(j.lUD) > j.axisTh):
            self["idleStanding"] = True
            
        #MOUSE MOVEMENT
        #calculate rotation
        rot = (self["prevMouseX"] - mouseX) / 300
        
        #checks prev mouse position with current and rotates player based on that
        if self["prevMouseX"] < mouseX:
            self.applyRotation((0, 0, rot), True)
            self["prevMouseX"] = mouseX
        
        if self["prevMouseX"] > mouseX:
            self.applyRotation((0, 0, rot), True)
            self["prevMouseX"] = mouseX
            
#-----------------------------------------------------------------------------------------------
            
        #THROW
        chair = None
        inRange = False
        selfHeight = 1.75
        self["holdingChair"]
            
        #check collision
        if self.sensors["chairCollision"].positive:
            chair = self.sensors["chairCollision"].hitObject
            inRange = True
            if k(events.SPACEKEY) or j.leftTrigger > 0:
                #set holdingChair to true
                self["holdingChair"] = True

        #hold chair        
        if self["holdingChair"]:
            #chair follows player and is above the players head
            chair.worldPosition = self.worldPosition
            chair.applyMovement((0, 0, selfHeight), False)
            chair.worldOrientation = self.worldOrientation
            #animations while holding chair
            if k(events.WKEY) or k(events.AKEY) or k(events.DKEY) or (abs(j.lLR) > j.axisTh or abs(j.lUD) > j.axisTh):
                self["walkingChair"] = True
            elif k(events.SKEY):
                self["backWalkingChair"] = True
            if not k(events.SKEY) and not k(events.WKEY) and not k(events.AKEY) and not k(events.DKEY) and not (abs(j.lLR) > j.axisTh or abs(j.lUD) > j.axisTh):
                self["idleChair"] = True

        #throw Chair
        if not (k(events.SPACEKEY) or (j.leftTrigger > 0)) and self["holdingChair"]:
            chair.restoreDynamics()
            chair.setLinearVelocity([0, -12, 3], True)
            chair = None
            self["holdingChair"] = False
            
            
        #kick chair                                           
        if k(events.LEFTSHIFTKEY) or j.rightTrigger > 0:
            if inRange:
                chair.worldOrientation = self.worldOrientation
                chair.setLinearVelocity([0, -7, 7], True)
            
def main (cont):
    
    own = cont.owner
    
    if not "init" in own:
        own["init"] = 1
        MovementAndThrow(own)
    else:
        own.main()