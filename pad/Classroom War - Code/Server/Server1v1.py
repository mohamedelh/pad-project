#De comments hier gelden ook voor server2v2 en server 3v3
#Importeer de libraries en de benodigde zelfgemaakte classes
from bge import logic
from chair import *
from entity import *
from remoteJoystick import *
from remoteKeyboard import *
from teamsystem import *
import socket
import pickle
        
class Server:
    #Set server up
    def __init__(self, host="localhost", port=9454):
        
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.setblocking(False)
        self.socket.bind((host, port))
        #Verzamel ip addressen
        self.addresses = []
        #Verzamel de spelers met bijbehorende avatars
        self.addres_entity = {}
        self.chairSpawn = None
        self.teamSystem = None
            
    def receive(self):
        
        
        while True:
            
            try:
                data, addres = self.socket.recvfrom(1024)
                
                if not addres in self.addres_entity:
                    entity = Entity(data.decode())
                    self.addres_entity[addres] = entity
                    self.addresses.append(addres)
                    #Speel telkens de game() methode af om te kijken of er 2 spelers zijn. Zo ja, dan gaat game() te werk
                    game()
                          
                else:
                    #Haal de gedrukte toetsen op
                    entity = self.addres_entity[addres]
                    entity.keyboard.updateState(pickle.loads(data)[0])
                    entity.joystick.updateButton(pickle.loads(data)[1])
                    entity.mouse.updateMousePosition(pickle.loads(data)[2])
                    entity.joystick.updateAxis(pickle.loads(data)[3])
                    
            except socket.error:
                break
            
    def game(self):
        
        if len(self.addres_entity) == 2:
            #Set up teams en avatars
            scene = logic.getCurrentScene()
            self.chairSpawn = Chair()
            self.teamSystem = TeamSystem(2)
            self.chairSpawn.generate(15)
            self.teamSystem.getEntitiesRandomnized(self.addres_entity)
            self.teamSystem.setTeamRedAndBlue()
            self.teamSystem.spawnTeamRed()
            self.teamSystem.spawnTeamBlue()
            self.teamSystem.givePlayerNameRed()
            self.teamSystem.givePlayerNameBlue()
            self.teamSystem.giveAvatarEntityRed()
            self.teamSystem.giveAvatarEntityBlue()
        
    def send(self):
        
        scene = logic.getCurrentScene()
        #Hier wordt alle data in verpakt en gestuurd naar server
        alldata = []
                
        state = {}
        avatar = {(gobj.name, gobj["entity"].name): [list(gobj.worldPosition), list(gobj.worldOrientation.to_euler()), gobj["playerWalking"], gobj["playerWalkBack"], gobj["idleStanding"], gobj["idleChair"], gobj["walkingChair"], gobj["backWalkingChair"]] \
                 for gobj in scene.objects \
                 if gobj.name == "Avatar" or gobj.name == "Avatar1"}
            
        chair = {(gobj.name, gobj["entity"].name): [list(gobj.worldPosition), list(gobj.worldOrientation.to_euler())] \
                 for gobj in scene.objects \
                 if gobj.name == "Chair"}
        
        
        state.update(avatar)
        state.update(chair)
        alldata.append(state)
        alldata.append([scene.objects["clock"]["start/stop"], scene.objects["clock"]["stoptijd"]])
        alldata.append(list(scene.objects["Camera"].worldPosition))
        #Restart server bestand als timer op 0 is
        if int(logic.getCurrentScene().objects['clock']['time']) == 0:
            self.socket.close()
            logic.restartGame()
            
        
        for addres in self.addres_entity:
            self.socket.sendto(pickle.dumps(alldata), addres)

server = Server()

def receive():
    
    server.receive()

def game():

    server.game()    
    
def send():
    
    server.send()
    
