from bge import logic
from entity import *

class Chair:
    
    def __init__(self):
        self.chairList = []
    
    def generate(self, size):
        
        scene = logic.getCurrentScene()
        spawner = scene.objects["stoelSpawn"]
        
        for x in range(size):
            chair = scene.addObject("Chair", spawner)
            chair["entity"] = Entity(x)
            self.chairList.append(chair)
            

    
