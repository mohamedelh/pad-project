from bge import logic
from chair import *
from entity import *
from remoteJoystick import *
from remoteKeyboard import *
from teamsystem import *
import socket
import pickle
        
class Server:
    
    def __init__(self, host="145.28.238.2", port=9567):
        
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.setblocking(False)
        self.socket.bind((host, port))
        self.addresses = []
        self.addres_entity = {}
        self.chairSpawn = Chair()
                
        
    def receive(self):
        
        
        while True:
            
            try:
                data, addres = self.socket.recvfrom(1024)
                
                if not addres in self.addres_entity:
                    entity = Entity(data.decode())
                    self.addres_entity[addres] = entity
                    self.addresses.append(addres)
                    game()
                          
                else:
                    entity = self.addres_entity[addres]
                    entity.keyboard.updateState(pickle.loads(data)[0])
                    entity.joystick.updateButton(pickle.loads(data)[1])
                    entity.mouse.updateMousePosition(pickle.loads(data)[2])
                    entity.joystick.updateAxis(pickle.loads(data)[3])
                    
            except socket.error:
                break
            
    def game(self):
        
        if len(self.addres_entity) == 4:
            scene = logic.getCurrentScene()
            
            self.teamSystem = TeamSystem(4)
            self.chairSpawn.generate(19)
            self.teamSystem.getEntitiesRandomnized(self.addres_entity)
            self.teamSystem.setTeamRedAndBlue()
            self.teamSystem.spawnTeamRed()
            self.teamSystem.spawnTeamBlue()
            self.teamSystem.givePlayerNameRed()
            self.teamSystem.givePlayerNameBlue()
            self.teamSystem.giveAvatarEntityRed()
            self.teamSystem.giveAvatarEntityBlue()
        
    def send(self):
        
        scene = logic.getCurrentScene()
        
        alldata = []
                
        state = {}
        avatar = {(gobj.name, gobj["entity"].name): [list(gobj.worldPosition), list(gobj.worldOrientation.to_euler()), gobj["playerWalking"], gobj["playerWalkBack"], gobj["idleStanding"], gobj["idleChair"], gobj["walkingChair"], gobj["backWalkingChair"]] \
                 for gobj in scene.objects \
                 if gobj.name == "Avatar" or gobj.name == "Avatar1"}
            
        chair = {(gobj.name, gobj["entity"].name): [list(gobj.worldPosition), list(gobj.worldOrientation.to_euler())] \
                 for gobj in scene.objects \
                 if gobj.name == "Chair"}
        
        
        state.update(avatar)
        state.update(chair)
        alldata.append(state)
        alldata.append([scene.objects["clock"]["start/stop"], scene.objects["clock"]["stoptijd"]])
        alldata.append(list(scene.objects["Camera"].worldPosition))
        
        if int(logic.getCurrentScene().objects['clock']['time']) == 0:
            self.socket.close()
            logic.restartGame()
            
        
        for addres in self.addres_entity:
            self.socket.sendto(pickle.dumps(alldata), addres)

server = Server()

def receive():
    
    server.receive()

def game():

    server.game()    
    
def send():
    
    server.send()
    
