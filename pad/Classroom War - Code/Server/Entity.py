from remoteKeyboard import *
from remoteJoystick import *
from remoteMouse import *

#Identificeert avatar voor bep. speler, zodat de avatar bestuurd kan worden
class Entity:
    
    def __init__(self, name):
        
        self.name = name
        self.keyboard = RemoteKeyboard()
        self.joystick = RemoteJoystick()
        self.mouse = RemoteMouse()