import bge
from datetime import datetime

def Clock(cn):
    # Get owner
    ow = cn.owner
    # if time is not started 
    if ow['start/stop'] == False:
        # Set time to -122 seconds
        ow['time'] = (-120.0)
        # Set text to ready
        ow['readout'] = '....ready'
        # if time is started
    if ow['start/stop'] == True:
        # create a time variable(the - is to make time negative)
        time = datetime.fromtimestamp((-ow["time"]))
        # the text format
        ow['readout'] = '{:%M:%S}'.format(time)
        # if time is finished
    if ow['finish'] == True:
        # set text to finishtime
        ow['readout'] = ow['finishtime']
        # set text to string readout property
    ow.text = str(ow["readout"])
    # when time is 2 sec
    if int(ow['time']) == 0:
        # stop the time
        ow['stoptijd'] = True
        print(ow['stoptijd'])

def Controller(cn):
    # get object owner
    ow = cn.owner
    # get scene
    scene = bge.logic.getCurrentScene()
    # make list
    playerList = []
    # loop through the array
    for x in scene.objects:
        # if the objectname is Avatar or Avatar1
        if x.name == 'Avatar' or x.name == 'Avatar1':
            # add it to the list
            playerList.append(x)
            
    # if there are two avatars in list        
    if len(playerList) >= 2:
        # Start the time
        ow['start/stop'] = True
            