from bge import logic
import random

class TeamSystem:
    
    def __init__(self, amount):
        
        self.amount = amount
        self.teamRed = []
        self.avatarIdentifierRed = []
        self.avatarIdentifierBlue = []
        self.teamBlue = []
        self.addressesRandomnized = []
        self.address_entityRandomnized = []
        self.redSpawners = []
        self.blueSpawners = []

    #Spelers worden willekeurig in team gezet  
    def getEntitiesRandomnized(self, entities):
        self.addressesRandomnized = list(entities.keys())
        random.shuffle(self.addressesRandomnized)
        self.address_entityRandomnized = [[address, entities[address]] for address in self.addressesRandomnized]
        
    def setTeamRedAndBlue(self):
        for x in range(self.amount):
            if x < (self.amount/2):
                self.teamRed.append(self.address_entityRandomnized[x])
            else:
                self.teamBlue.append(self.address_entityRandomnized[x])
                print(self.teamBlue)

    def spawnTeamRed(self):
        
        scene = logic.getCurrentScene()
        
        self.redSpawners = [scene.objects["Spawner0"], scene.objects["Spawner1"], scene.objects["Spawner2"]]
        
        for x in range(len(self.teamRed)):
            self.avatarIdentifierRed.append(scene.addObject("Avatar1", self.redSpawners[x]))
    
    def spawnTeamBlue(self):
        
        scene = logic.getCurrentScene()
        
        self.blueSpawners = [scene.objects["Spawner3"], scene.objects["Spawner4"], scene.objects["Spawner5"]]
        print(len(self.blueSpawners))
        
        for x in range(len(self.teamBlue)):
            self.avatarIdentifierBlue.append(scene.addObject("Avatar", self.blueSpawners[x]))
        

    def givePlayerNameRed(self):
        
        for x in range(len(self.teamRed)):
            self.avatarIdentifierRed[x].children[0]["Text"] = self.teamRed[x][1].name
            
    def givePlayerNameBlue(self):

        for x in range(len(self.teamBlue)):
            self.avatarIdentifierBlue[x].children[0]["Text"] = self.teamBlue[x][1].name
                    
    def giveAvatarEntityRed(self):
        
        for x in range(len(self.teamRed)):
            self.avatarIdentifierRed[x]["entity"] = self.teamRed[x][1]
            
    def giveAvatarEntityBlue(self):
        
        for x in range(len(self.teamBlue)):
            self.avatarIdentifierBlue[x]["entity"] = self.teamBlue[x][1]