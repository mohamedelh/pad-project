class RemoteJoystick:
    
    
    def __init__(self):
        
        self.list_button_stat = []
        self.list_axisValues = []
        self.aBut = 0
        self.bBut = 1
        self.xBut = 2
        self.yBut = 3
        self.startBut = 6
        self.leftShoulderBut = 9
        self.rightShoulderBut = 10
        self.dpadUpBut = 11
        self.dpadDownBut = 12
        self.dpadLeftBut = 13
        self.dpadRightBut = 14
        self.lLR = 0
        self.lUD = 0
        self.rLR = 0
        self.rUD = 0
        self.reduction = 400000 
        self.axisTh = 0.03 
        
    def updateButton(self, list_button_stat):
        
        self.list_button_stat = list_button_stat        
    
    def updateAxis(self, list_axisValues):
        
        self.list_axisValues = list_axisValues
        if len(self.list_axisValues) == 6:
            self.lLR = self.list_axisValues[0]
            self.lUD = self.list_axisValues[1]
            self.rLR = self.list_axisValues[2]
            self.rUD = self.list_axisValues[3]
            self.leftTrigger = self.list_axisValues[4]
            self.rightTrigger = self.list_axisValues[5]
            
    def joystickDown(self, button):
        if button in self.list_button_stat:
            return True
        
        return False