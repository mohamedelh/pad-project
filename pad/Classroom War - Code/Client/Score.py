from bge import logic 

class Score:
    
    def __init__(self):
        
    # get scene
        self.scene = logic.getCurrentScene()
        # get a list of objects in scene
        self.objList = self.scene.objects
        # get controller
        self.scoreText = self.scene.objects["Score"]
        
        self.chairList = []
        self.blueScore = []
        self.redScore = []        

    def main(self):
        
        #checks if gameobject is Chair
        for gobj in self.objList:
                if gobj.name == "Chair":
                    self.chairList.append(gobj)
        
        #tracks position off chair object and puts it in a list according to its position
        for chair in self.chairList:
            if chair.worldPosition.y > 0:
                self.blueScore.append(chair)
            else:
                self.redScore.append(chair)
        
        #turns the lenght of the list of chairs into a string        
        blue = str(len(self.blueScore))
        red = str(len(self.redScore))
        
        #makes textobject display score        
        self.scoreText.text = (red + " - " + blue)
     
    #returns winner   
    def getWinner(self):
        
        if str(len(self.blueScore)) > str(len(self.redScore)):
            return "Red"
        else:
            return "Blue"
    
    
Score().main()