import bge
from datetime import datetime
from score import *

def Clock(cn):
    # get owner
    ow = cn.owner
    # if time is not started
    if ow['start/stop'] == False:
        # set time to -120
        ow['time'] = (-120.0)
        # set text to ready
        ow['readout'] = '....ready'
    # if time is started    
    if ow['start/stop'] == True:
        # creat a time object
        time = datetime.fromtimestamp(-ow["time"])
        # format the text object
        ow['readout'] = '{:%M:%S}'.format(time)
    # if time is finished    
    if ow['finish'] == True:
        # set text to finishtime
        ow['readout'] = ow['finishtime']
    # set text to the string property readout    
    ow.text = str(ow["readout"])
    # if time is finished in the server
    if ow['stoptijd']:
        # if team blue won
        if Score().getWinner() == 'Blue':
            # activate actuator Blue
            cn.activate("Blue")
        # if team red won
        else:
            # activate actuator Red
            cn.activate("Red")

    

