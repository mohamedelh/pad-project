#Importeer benodigde libraries
from bge import logic, events
import socket
from datetime import datetime
from mathutils import Vector
from score import *
import pickle
import random

#kijkt of er inderdaad op een bepaalde knop is gedrukt
def joystickDown(button):
    buttons = logic.getCurrentController().sensors["Button"]
    if button in buttons.getButtonActiveList:
        return True
    return False

#Verwijdert de 1v1/2v2/3v3 interface
def removeObjects():
    scene = logic.getCurrentScene()
    play1v1= scene.objects["play1v1"]
    play2v2= scene.objects["play2v2"]
    play3v3= scene.objects["play3v3"]
    plane = scene.objects["Plane.004"]
    play1v1.endObject()
    play2v2.endObject()
    play3v3.endObject()
    plane.endObject()

#Zorgt ervoor dat er voor 1v1/2v2/3v3 kan worden gekozen
def getPort(): 
    scene = logic.getCurrentScene()
    
    for x in scene.objects:
        if x.name == "play1v1" or x.name == "play2v2" or x.name == "play3v3":
            if scene.objects["play1v1"].sensors["Mouse"].hitObject == scene.objects["play1v1"] and scene.objects["play1v1"].sensors["Mouse.001"].positive or scene.objects["Camera"]["1v1check"]:
                removeObjects()
                scene.objects["Camera"]["1v1check"] = True
                scene.objects["Camera"].worldPosition = [26, 0, 21]
                scene.objects["Camera"].worldOrientation = [[-0, -0.5736, 0.8192],[ 1, -0, 0],[-0,  0.8192, 0.5736]]
                
            if scene.objects["play2v2"].sensors["Mouse"].hitObject == scene.objects["play2v2"] and scene.objects["play2v2"].sensors["Mouse.001"].positive or scene.objects["Camera"]["2v2check"]:
                removeObjects()
                scene.objects["Camera"]["2v2check"] = True
                scene.objects["Camera"].worldPosition = [26, 0, 21]
                scene.objects["Camera"].worldOrientation = [[-0, -0.5736, 0.8192],[ 1, -0, 0],[-0,  0.8192, 0.5736]]
        
            if scene.objects["play3v3"].sensors["Mouse"].hitObject == scene.objects["play3v3"] and scene.objects["play3v3"].sensors["Mouse.001"].positive or scene.objects["Camera"]["3v3check"]:
                removeObjects()
                scene.objects["Camera"]["3v3check"] = True
                scene.objects["Camera"].worldPosition = [26, 0, 21]
                scene.objects["Camera"].worldOrientation = [[-0, -0.5736, 0.8192],[ 1, -0, 0],[-0,  0.8192, 0.5736]]
    
    #Hier wordt de juiste port meegestuurd op basis van wat true is
    if scene.objects["Camera"]["1v1check"]:
        return 9454
    if scene.objects["Camera"]["2v2check"]:
        return 9567                
    if scene.objects["Camera"]["3v3check"]:
        return 9567


        
    
#Pakt de waarde van de left/right thumbsticks en triggers    
def getAxisValues():
    reduction = 400000
    axisTh = 0.03 
    aXis = logic.getCurrentController().sensors["Axis"]
    axisList = []
    # left and right on the left thumb stick = 0 on Linux and Windows;
    lar_lts = 0
    # up and down on the left thumb stick = 1 on Linux and Windows;
    uad_lts = 1
    # left and right on the right thumb stick = 3 on Linux, but = 2 on Windows;
    lar_rts = 3
    # up and down on the right thumb stick = 4 on Linux, but = 3 on Windows;
    uad_rts = 4 
    
    # left trigger = 2 on Linux, but = 4 on Windows;
    lt = 2
    # right trigger = 5 on Linux and Windows
    rt = 5
    
    if aXis.connected:
        lLR = aXis.axisValues[lar_lts] / reduction
        lUD = aXis.axisValues[uad_lts] / reduction
        rLR = aXis.axisValues[lar_rts] / reduction
        rUD = aXis.axisValues[uad_rts] / reduction
        lTrig = aXis.axisValues[lt] / reduction
        rTrig = aXis.axisValues[rt] / reduction
    
        axisList = [lLR, lUD, rLR, rUD, lTrig, rTrig]
    
    return axisList
    
#Kijkt of er inderdaad op een bepaalde knop op het toetsenbord is gedrukt    
def keyDown(key_code, status=logic.KX_INPUT_ACTIVE):
    if logic.keyboard.events[key_code] == status:
        return True
    return False

#Wordt gebruikt om te kijken OF er op een knop wordt gedrukt, voor if condities
def keyHit(key_code):
    return keyDown(key_code, logic.KX_INPUT_JUST_ACTIVATED)

#Wordt gebruikt om te kijken OF er op een knop wordt gedrukt, voor if condities
def joystickHit(button):
    return joystickDown(button)
        
class Client:
    
    def __init__(self, server_ip="localhost"):
        self.ip = server_ip
        self.port = None
        self.server_addres = (server_ip, self.port)
        #UDP Socket wordt geopend
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.setblocking(False)
        #Hier worden de avatars en stoelen verzameld
        self.entities = {}
        #Eerst wordt de naam gestuurd naar server
        self.main = self.state_sendName 
        
    def setSocket(self):
        
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.setblocking(False) 
        
    def setAddres(self, port):
        scene = logic.getCurrentScene()
        if port is not None and (scene.objects["Camera"]["1v1check"] or scene.objects["Camera"]["2v2check"] or scene.objects["Camera"]["3v3check"]):
            self.server_addres = (self.ip, port)
            self.port = port      
        
    def state_sendName(self):
        
        scene = logic.getCurrentScene()
        text = scene.objects["Name.001"]
        textn = scene.objects["TextName"]
        
        if self.port is not None:
            if keyHit(events.ENTERKEY):
                print(bytes(text["Text"], "utf-8"))
                self.socket.sendto(bytes(text["Text"], "utf-8"), self.server_addres)
                print(self.server_addres)
                text.endObject()
                textn.endObject()
                #dan wordt de state_loop methode geroepen. Dit wordt continu geroepen door always sensor
                self.main = self.state_loop
            
    def state_loop(self):
        self.send()
        self.receive()
    
    #de ingedrukte knoppen worden meegestuurd    
    def send(self):
        list_all_stat = []
        list_key_stat = []
        list_button_stat = []
        list_mouse_stat = []
        list_axisValues = []
        
        keyboard_events = logic.keyboard.events
        for k in keyboard_events:
            s = keyboard_events[k]
            if s in (logic.KX_INPUT_JUST_ACTIVATED, logic.KX_INPUT_JUST_RELEASED):
                list_key_stat.append((k, s))
        
        list_mouse_stat = logic.getCurrentController().sensors["Mouse"].position
        
        list_button_stat = logic.getCurrentController().sensors["Button"].getButtonActiveList()
                
        list_axisValues = getAxisValues()
        
        list_all_stat.append(list_key_stat)
        list_all_stat.append(list_button_stat)
        list_all_stat.append(list_mouse_stat)
        list_all_stat.append(list_axisValues)
        
            
        if len(list_all_stat):
            self.socket.sendto(pickle.dumps(list_all_stat), self.server_addres)
                
        
    def receive(self):
        scene = logic.getCurrentScene()
        
        while True:
            
            try:
                #Ontvangt data van server(De alldata list die van server werd gestuurd)
                data, addres = self.socket.recvfrom(4096)
                #state is de dictionary die in alldata list zat. Met de avatars en stoelen
                state = pickle.loads(data)[0] 
                #In client worden de avatars en stoelen opnieuw gespawned op basis van de informatie in dictionary
                for k in state:
                    
                    if not k in self.entities:
                        
                        if k[0] == "Chair":
                            scene = logic.getCurrentScene()
                            stoelspawn = scene.objects["stoelSpawn"]
                            entity = scene.addObject("Chair", stoelspawn)
                            self.entities[k] = entity
                            
                        if k[0] == "Avatar":
                        
                            scene = logic.getCurrentScene()
                            spawner = scene.objects["Spawner5"]
                        
                            avatar = scene.addObject("Avatar", spawner)
                            name = scene.addObject("Name")
                            name["Text"] = k[1][:-1]
                            entity = [avatar, name]
                            self.entities[k] = entity
                            
                        if k[0] == "Avatar1":
                            
                            scene = logic.getCurrentScene()
                            spawner = scene.objects["Spawner1"]
                            
                            avatar = scene.addObject("Avatar1", spawner) 
                            name = scene.addObject("Name")
                            name["Text"] = k[1][:-1]
                            entity = [avatar, name]
                            self.entities[k] = entity
                                                  
                    else:
                        entity = self.entities[k]
                    
#---------------------------------------------------------------------------------------------------                    
#Animation                    
                    if k[0] == "Avatar" or k[0] == "Avatar1":
                        if state[k][2]:
                            entity[0].playAction("Walking", 0, 59, blendin=5, speed=2, play_mode=1)
                        if state[k][3]:
                            entity[0].playAction("Walking", 59, 0, blendin=5, speed=2, play_mode=1)
                        if state[k][4]:
                            entity[0].playAction("standing", 0, 60, blendin=5, speed=0.7, play_mode=1)

                        if state[k][5]:
                            entity[0].playAction("standingChair2", 0, 60, blendin=5, speed=0.7, play_mode=1)
                        if state[k][6]:
                            entity[0].playAction("WalkingChair", 0, 59, blendin=5, speed=2, play_mode=1)
                        if state[k][7]:
                            entity[0].playAction("WalkingChair", 59, 0, blendin=5, speed=2, play_mode=1)
#---------------------------------------------------------------------------------------------------                        
#player position and rotation and name of player position
                    if k[0] == "Avatar" or k[0] == "Avatar1":
                        entity[0].worldPosition = Vector(state[k][0])
                        entity[1].worldPosition[0] = Vector(state[k][0])[0]
                        entity[1].worldPosition[1] = entity[1].worldPosition[1] = Vector(state[k][0])[1] - (len(entity[1].text) * 0.15)
                        entity[0].worldOrientation = Vector(state[k][1])
                    else:
                        entity.worldPosition = Vector(state[k][0])
                        entity.worldOrientation = Vector(state[k][1])                        
                    
                    logic.getCurrentScene().objects["clock"]["start/stop"] = pickle.loads(data)[1][0]
                    logic.getCurrentScene().objects["clock"]["stoptijd"] = pickle.loads(data)[1][1]
                    logic.getCurrentScene().objects["Camera"].worldPosition = pickle.loads(data)[2]
                    
                    #dit is logic.getCurrentScene().objects["clock"]["stoptijd"]. Als het true is, gaat de client weg van de server
                    if pickle.loads(data)[1][1]:
                        self.socket.close()
                        self.server_port = 9874
                    
 
            except socket.error:
                break  

client = Client() 
    
def main():
    port = getPort() 
    #Camera checker wodt weer false gezet zodat port dit gedeelte het adres van de client niet steeds opnieuw wordt gezet
    if port is not None and logic.getCurrentScene().objects["Camera"]["checker"]:       
        client.setAddres(port)
        logic.getCurrentScene().objects["Camera"]["checker"] = False
    #Client main runt pas als de de client object een port heeft
    if client.port is not None:    
        client.main() 